import argparse


def parse_arguments():
    usage_options = "%(prog)s [-f] "

    parser = argparse.ArgumentParser(
        usage=usage_options,
        description="<command description>"
    )

    parser.add_argument("-f", "--file", default="aminoacidos.csv")

    return parser.parse_args()
