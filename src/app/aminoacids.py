import args
import dna
import csv
import json

def get_aminoacids_properties():
    arguments = args.parse_arguments()

    aminoacids = {}

    ## lee el csv y lo imprime
    with open('./src/app/aminoacids.csv', 'r') as file:
        reader = csv.reader(file, quotechar='"')
        next(reader)
        for row in reader:
            aminoacid = {
              'name': row[0],
              'identifier': row[1],
              'mw': row[2],
              'pi': row[3],
              'polarity': row[4],
              'charge':  row[5]
            }
            aminoacids[row[1]] = aminoacid
            #print(aminoacid)
    
    file.close()
    return aminoacids


def get_aminoacids_strand():
    strandAminioAcids = []

    strandDNA = dna.get_strand()
    jsonFile = open('./src/app/aminoacidsDNA.json',)
    adminoAcidsMap = json.load(jsonFile)
    for codon in strandDNA:
        aminoAcid = adminoAcidsMap[codon.upper()] if codon.upper() in adminoAcidsMap else adminoAcidsMap[codon.isupper()]
        strandAminioAcids.append(aminoAcid)

    return strandAminioAcids
