from decimal import Decimal
def calculate(input_protein, aminoacids_properties):
	mass = 0
	p_isolectric = 0
	no_polar = 0
	polar = 0
	for aminoacid in input_protein:
		if(aminoacid != 'STOP'):
			properties = aminoacids_properties[aminoacid]
			mass += Decimal(properties['mw'])
			p_isolectric += Decimal(properties['pi'])
			if(properties['polarity'] == 'no-polar'):
				no_polar += 1
			if(properties['polarity'] == 'polar'):
				polar += 1

	p_isolectric = p_isolectric / len(input_protein)
	result = (mass, p_isolectric, 'polar')
	
	if no_polar > polar :
		result[2] = 'no_polar'

	return result
