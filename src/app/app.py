import aminoacids
import calculate_avg
import predict_filter

def print_report(data, result):
    print('##########################################')
    print('Resultado del procesamiento de la proteina')
    print('Peso Molecular: ' + str(round(data[0], 2)))
    print('Punto Isoeléctrico: ' + str(round(data[1], 2)))
    print('Polaridad: ' + data[2])
    print('------------------------------------------')
    print('Sistema de Filtrado: ' + result)
    print('##########################################')
    print('')

def main():
    strandAminoAcids = aminoacids.get_aminoacids_strand()
    amninoAcidsProperties = aminoacids.get_aminoacids_properties()
    data_parameters = calculate_avg.calculate(strandAminoAcids, amninoAcidsProperties)
    filter = predict_filter.predict_filter(data_parameters)
    print_report(data_parameters, filter)

if __name__ == "__main__":
    main()


