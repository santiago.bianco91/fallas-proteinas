def predict_filter(parameters):
    mass = parameters[0]
    #print (mass)
    isoelectric_bridge = parameters[1]
    #print(isoelectric_bridge)
    polarity = parameters[2]
    #print(polarity)

    is_acid = (isoelectric_bridge < 7)
    is_basic = (isoelectric_bridge >= 7)
    is_polar = (polarity == "polar")
    is_not_polar = (polarity == "no-polar")
    is_small = (mass < 22000)
    is_medium = (22000 < mass < 44000)
    is_big = (mass > 44000)

    if(is_acid and is_polar and is_small):
        return "ACP"
    elif(is_basic and is_not_polar and is_small):
        return "BCNP"
    elif(is_acid and is_not_polar and is_medium):
        return "AMNP"
    elif(is_basic and is_polar and is_medium):
        return "BMP"
    else:
        return "Sistema personalizado"
