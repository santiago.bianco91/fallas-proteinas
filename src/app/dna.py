import args
import csv

def get_strand():
    arguments = args.parse_arguments()
    strand = []

    ## lee el txt de la cadena de adn
    with open(arguments.file, 'r') as file:
      data = file.read().strip().replace('\n', '')
      strand = data.split(" ")
    
    file.close()

    return strand
